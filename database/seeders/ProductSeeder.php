<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre'=>'Disco Duro 1TG',
            'sku'=>100,
            'descripcion'=>' TOSHIBA 1 TB'
        ]);

        DB::table('products')->insert([
            'nombre'=>'DISCOS SOLIDOS SSD 1TG',
            'sku'=>100,
            'descripcion'=>' SSD M.2 NVME 1000GB'
        ]);

        DB::table('products')->insert([
            'nombre'=>'PROCESADORES INTEL',
            'sku'=>100,
            'descripcion'=>' CORE I3 8100 3.6 CACHE 6MB LGA 1151 8 GEN'
        ]);
    }
}
