<?php

namespace App\Http\Controllers;

use App\Models\Product_Cars;
use Illuminate\Http\Request;
use DB;

class ProductCarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json ( Product_Cars::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json ( Product_Cars::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_Cars  $product_Cars
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $carrito=DB::table('product__cars')
            ->where('product__cars.cart_id',$id)
            ->join('products','products.id',"=","product__cars.product_id")
            ->select('product__cars.id','product__cars.quantity','product__cars.product_id','products.nombre','descripcion')
           ->get();

        return  response()->json ($carrito );
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_Cars  $product_Cars
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $input= Product_Cars::find($id);
            $input->product_id= $request->product_id;
            $input->cart_id= $request->cart_id;
            $input->quantity= $request->quantity;
            return response()->json ($input->save());
          // return response()->json ($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_Cars  $product_Cars
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $input= Product_Cars::find($id);
        return $input->delete();
    }
}
