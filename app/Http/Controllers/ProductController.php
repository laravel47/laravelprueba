<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json ( Product::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json ( Product::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  response()->json ( Product::find($id));
    }

    public function updates( $obj,  $id)
    {
        try{
            $input= Product::find($id);
            $input->nombre= $obj->nombre;
            $input->sku= $obj->sku;
            $input->descripcion= $obj->descripcion;
            return response()->json ($input->save());
        }catch(Exception $ex){
            return response()->json ($ex);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        try{
            $input= Product::find($id);
            $input->nombre= $request->nombre;
            $input->sku= $request->sku;
            $input->descripcion= $request->descripcion;
            return response()->json ($input->save());
        }catch(Exception $ex){
            return response()->json ($ex);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $input= Product::find($id);
        return $input->delete();
    }
}
