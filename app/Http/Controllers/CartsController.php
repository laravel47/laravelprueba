<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use Illuminate\Http\Request;

class CartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json ( Carts::all());
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json ( Carts::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Carts  $carts
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        return  response()->json ( Carts::find($id));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Carts  $carts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input= Carts::find($id);
        $input->status= $request->status;
        $result=$input->save();
        $prodCar = new ProductCarsController();
        $prod = new ProductController();
        $consulta=$prodCar->show($id);

        foreach($consulta->original as $item){

            $finproducto= $prod->show($item->product_id);
            $sku= intval($item->quantity) - intval($finproducto->original->sku);
            $object = (object) [
                'nombre' => $finproducto->original->nombre,
                'sku' => $sku,
                'descripcion' => $finproducto->original->descripcion,
              ];
            $update= $prod->updates($object, $item->product_id);
        }

        return response()->json ($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Carts  $carts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carts $carts)
    {
        //
    }
}
